# Yuwen_IDS721_Mini5: Serverless Rust Microservice

A serverless Rust Lambda function that manages to choose a random song given artist name from the AWS DynamoDB


## :ballot_box_with_check: Requirements
* Create a Rust AWS Lambda function (or app runner)
* Implement a simple service
* Connect to a database


## :ballot_box_with_check: Main Progress
### __`Create the Database`__ 

[AWS Developer Guide](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStartedDynamoDB.html)


Step 1: Create a table

Step 2: Write data to a table using the console or AWS CLI

My table is a music table following the developer guide with 

        ```
        Partition key — Artist
        Sort key — SongTitle
        ```

![database](imgs/dynamodb.png)


###  __`Create the a Rust Lambda function`__

1. Use Cargo Lambda to create the new project

    ```
    cargo lambda new <projectName> && cd <projectName>
    ```

2. Modify codes in `src/main.rs` and `Cargo.toml` to accommodate with your functionality and corresponding dependencies.

    For my project, I am working on connecting with my DynamoDB database and fetech a random song from a given artist

3. Test functionality locally
    ```
    cargo lambda watch
    cargo lambda invoke --data-ascii "{ \"artist\": \"Seventeen\" }"
    ```

    ![local_test](imgs/invoke.png)

4. Build the function to deploy it on AWS Lambda
    ```
    cargo lambda build --release
    cargo lambda deploy
    ```
    ![lambda](imgs/lambda_with_api.png)

5. Optional: Create an API gateway to trigger the Lambda function

###  __`Test Lambda Functionality in Test Event`__

In the test section under Lambda, create a test event to make sure this function can perform correctly.
Test Event:
![test_event](imgs/test_event.png)

Test Result:
![test_result](imgs/test_result.png)