// Import necessary libraries and modules
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

// Define a struct to represent the request payload received by the Lambda function
#[derive(Deserialize)]
struct Request {
    artist: Option<String>,
}

// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    song_title: String,
}

// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

// The handler function responsible for processing Lambda events
async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    // Deserialize the incoming JSON payload into the Request struct
    let request: Request = serde_json::from_value(event.payload)?;

    // Load AWS configuration from environment variables
    let config = load_from_env().await;
    // Create a DynamoDB client using the loaded configuration
    let client = Client::new(&config);

    // Invoke the search_song function to retrieve a random song title based on the artist
    let song_title = search_song(&client, request.artist).await?;

    // Serialize the response into JSON format and return it
    Ok(json!({
        "SongTitle": song_title,
    }))
}

// Function to search for a random song title in DynamoDB based on the provided artist
async fn search_song(client: &Client, artist: Option<String>) -> Result<String, LambdaError> {
    // Define the DynamoDB table name
    let table_name = "Music";
    // Initialize HashMap for expression attribute values and a String for filter expression
    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();

    // Check if artist parameter is provided in the request
    if let Some(artist_val) = artist {
        // Insert artist parameter into expression attribute values
        expr_attr_values.insert(":artist_val".to_string(), AttributeValue::S(artist_val));
        filter_expression.push_str("Artist = :artist_val");
    }

    // Perform a DynamoDB scan operation with the constructed parameters
    let result = client.scan()
        .table_name(table_name)
        .set_expression_attribute_values(Some(expr_attr_values))
        .set_filter_expression(Some(filter_expression))
        .send()
        .await?;

    // Extract the items from the scan result
    let items = result.items.unwrap_or_default();

    // Randomly choose an item from the scan result
    let selected_item = items.iter().choose(&mut thread_rng());

    // Match and extract the song title from the selected item
    match selected_item {
        Some(item) => {
            let song_title_attr = item.get("SongTitle").and_then(|val| val.as_s().ok());

            // Match the extracted song title and return the result
            match song_title_attr {
                Some(song_title) => Ok(song_title.to_string()),
                None => Err(LambdaError::from("No SongTitle found in the selected item")),
            }
        },
        None => Err(LambdaError::from("No matching song found")),
    }
}
